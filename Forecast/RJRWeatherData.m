//
//  RJRWeatherData.m
//  Forecast
//
//  Created by Robert Randell on 11/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRWeatherData.h"
#import "RJRConversions.h"

@interface RJRWeatherData () <CLLocationManagerDelegate>

@property (nonatomic) RJRConversions *convertDataClass;
@property (nonatomic) CLLocationManager *locationManager;

@end

@implementation RJRWeatherData

- (id)initWithAPIKey:(NSString *)api_key {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.apiKey = [api_key copy];
    self.weatherDataFromAPI = [[NSMutableDictionary alloc] init];
    NSLog(@"Data Class Init... with %@", api_key);
    
    return self;
}

- (void)getLocation {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = 1000;
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationUpdateWithLatitude:(double)latitude withLongitude:(double)longitude {
    
    [self getFixedLocationAtLatitude:latitude atLongitude:longitude];
    
    NSLog(@"Fix: %f, %f", latitude, longitude);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [locations lastObject];
    
    double currentLatitude = location.coordinate.latitude;
    double currentLongitude = location.coordinate.longitude;
    
    self.locationLabel = @"Fetching";
    self.locationRegionLabel = @"Location";
    self.placeName = @"";
    self.regionName = @"";
    
    [self.locationManager stopUpdatingLocation];
    
    NSLog(@"Latitude: %f, Longitude: %f", currentLatitude, currentLongitude);
    
    [self getFixedLocationAtLatitude:currentLatitude atLongitude:currentLongitude];
            
}

- (void)getFixedLocationAtLatitude:(double)latitude atLongitude:(double)longitude {
    
    // ==== GeoCoder ====
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];

    CLLocationCoordinate2D coordinate;
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    
    CLLocation *currentLocation = [[CLLocation alloc] initWithCoordinate:coordinate altitude:1 horizontalAccuracy:1 verticalAccuracy:-1 timestamp:[NSDate date]];
    
    [geoCoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        if (error || placemarks.count == 0) {
            self.locationLabel = @"Sorry...";
            self.locationRegionLabel = @"Location not available.";
        }
        else {
            CLPlacemark *placemark = [placemarks firstObject];
            self.placeName = placemark.subLocality;
            self.regionName = placemark.subAdministrativeArea;
            
            if (self.placeName == NULL) {
                self.placeName = placemark.thoroughfare;
            }
            
            self.locationLabel = [NSString stringWithFormat:@"%@", self.placeName];
            self.locationRegionLabel = [NSString stringWithFormat:@"%@", self.regionName];
                        
            [self.weatherDataDelegate updateLocationLabel:self.placeName locationRegionLabel:self.regionName];
            
        }
    }];
        
    [self initQueryForDataLatitude:latitude longitude:longitude];
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    NSLog(@"Location Manager Failed With Error: %@", error);
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Weather is currently unavailable" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    
}
        
- (void)initQueryForDataLatitude:(double)latitude longitude:(double)longitude {
    
    NSLog(@"Getting Data for %f, %f", latitude, longitude);
    
    [self getCurrentConditionsForLatitude:latitude longitude:longitude success:^(NSMutableDictionary *responseDict) {
        self.weatherDataFromAPI = responseDict;
        [self passUpdatedDictionary:self.weatherDataFromAPI];
        //NSLog(@"%@", self.weatherDataFromAPI);
    } failure:^(NSError *error) {
        NSLog(@"%@", error.description);
    }];
}

- (void)getCurrentConditionsForLatitude:(double)lat longitude:(double)lon success:(void(^)(NSMutableDictionary *responseDict))success failure:(void(^)(NSError *error))failure {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.forecast.io/forecast/%@/%.6f,%.6f", self.apiKey, lat, lon]];
    //NSLog(@"https://api.forecast.io/forecast/%@/%.6f,%.6f", self.apiKey, lat, lon);
    
    self.manager = [AFHTTPSessionManager manager];
    
    [self.manager GET:[url absoluteString] parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        //success([NSMutableDictionary dictionaryWithDictionary:[responseObject objectForKey:@"currently"]]);
        success([NSMutableDictionary dictionaryWithDictionary:responseObject]);
        //NSLog(@"%@", responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)passUpdatedDictionary:(NSMutableDictionary *)data {
    
    NSLog(@"Passing data...");
    
    self.convertDataClass = [[RJRConversions alloc] init];
    
    // ==== Current Conditions ====
    UIImage *backgroundImage = [self.convertDataClass setIconImages:[[data objectForKey:@"currently"] objectForKey:@"icon"] iconSize:@"background"];
    UIImage *backgroundImageBlur = [self.convertDataClass setIconImages:[[data objectForKey:@"currently"] objectForKey:@"icon"] iconSize:@"background-blur"];
    UIImage *mainIconImage = [self.convertDataClass setIconImages:[[data objectForKey:@"currently"] objectForKey:@"icon"] iconSize:@"lrg"];
    
    NSString *apparentTemperatureString = [NSString stringWithFormat:@"%.0fC", [self.convertDataClass convertToCelsius:[[[data objectForKey:@"currently"] objectForKey:@"apparentTemperature"] doubleValue]]];
    NSString *humidityLevelString = [NSString stringWithFormat:@"Humidity: %.0f%%", [self.convertDataClass convertToPercentage:[[[data objectForKey:@"currently"] objectForKey:@"humidity"] doubleValue]]];
    NSString *iconLabelString = [NSString stringWithFormat:@"%@", [self.convertDataClass setCurrentCondition:[[data objectForKey:@"currently"] objectForKey:@"icon"]]];
    NSString *timeLabelString = [NSString stringWithFormat:@"%@", [self.convertDataClass convertFromUNIX:[[[data objectForKey:@"currently"] objectForKey:@"time"] doubleValue]]];
    NSString *windSpeedLabelString = [NSString stringWithFormat:@"Speed: %.0f mph", [[[data objectForKey:@"currently"] objectForKey:@"windSpeed"] doubleValue]];
    NSString *windDirLabelString = [NSString stringWithFormat:@"Direction: %@", [self.convertDataClass setWindDirection:[[[data objectForKey:@"currently"] objectForKey:@"windBearing"] doubleValue]]];
    NSString *nextDayLabelString = [[self.convertDataClass getAdvanceDays:[[[data objectForKey:@"currently"] objectForKey:@"time"] doubleValue] advance:2] uppercaseString];
    NSString *secondDayLabelString = [[self.convertDataClass getAdvanceDays:[[[data objectForKey:@"currently"] objectForKey:@"time"] doubleValue] advance:3] uppercaseString];
    NSString *pressureLabelString = [NSString stringWithFormat:@"Pressure: %@ mBar", [[data objectForKey:@"currently"] objectForKey:@"pressure"]];
    
    NSMutableArray *currentConditionsArray = [[NSMutableArray alloc] initWithObjects:apparentTemperatureString, humidityLevelString, iconLabelString, timeLabelString, windSpeedLabelString, windDirLabelString, nextDayLabelString, secondDayLabelString, pressureLabelString, backgroundImage, backgroundImageBlur, mainIconImage, nil];
    
    //NSLog(@"%@", currentConditionsArray);
    
    [self.weatherDataDelegate passArrayForCurrentConditions:currentConditionsArray];
    [self.weatherDataDelegate animateWindmill:[[[data objectForKey:@"currently"] objectForKey:@"windSpeed"] doubleValue]];
    
    // ==== Daily Conditions ====
    NSString *maxTemp = [NSString stringWithFormat:@"%.0fC", [self.convertDataClass convertToCelsius:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"apparentTemperatureMax"] doubleValue]]];
    NSString *minTemp = [NSString stringWithFormat:@"%.0fC", [self.convertDataClass convertToCelsius:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"apparentTemperatureMin"] doubleValue]]];
    NSString *sunRiseTime = [NSString stringWithFormat:@"%@", [self.convertDataClass convertToHourFromUNIX:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"sunriseTime"] doubleValue]]];
    NSString *sunSetTime = [NSString stringWithFormat:@"%@", [self.convertDataClass convertToHourFromUNIX:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"sunsetTime"] doubleValue]]];
    NSString *moonPhaseText = [NSString stringWithFormat:@"%@", [self.convertDataClass convertMoonPhase:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"moonPhase"] doubleValue]]];
    
    UIImage *moonPhaseIcon = [self.convertDataClass convertMoonPhaseIcon:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"moonPhase"] doubleValue]];
    UIImage *rainProbToday = [self.convertDataClass maskImage:[UIImage imageNamed:@"water.png"] withMask:[UIImage imageNamed:@"rainDropMask.png"]];
    UIImage *rainProbTomorrow = [self.convertDataClass maskImage:[UIImage imageNamed:@"water.png"] withMask:[UIImage imageNamed:@"rainDropMask.png"]];
    UIImage *rainProbDayTwo = [self.convertDataClass maskImage:[UIImage imageNamed:@"water.png"] withMask:[UIImage imageNamed:@"rainDropMask.png"]];
    UIImage *rainProbDayThree = [self.convertDataClass maskImage:[UIImage imageNamed:@"water.png"] withMask:[UIImage imageNamed:@"rainDropMask.png"]];
    
    NSString *rainProbTodayValue = [NSString stringWithFormat:@"%.0f%%", [self.convertDataClass convertToPercentage:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"precipProbability"] doubleValue]]];
    NSString *rainProbTomorrowValue = [NSString stringWithFormat:@"%.0f%%", [self.convertDataClass convertToPercentage:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:1] objectForKey:@"precipProbability"] doubleValue]]];
    NSString *rainProbDayTwoValue = [NSString stringWithFormat:@"%.0f%%", [self.convertDataClass convertToPercentage:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:2] objectForKey:@"precipProbability"] doubleValue]]];
    NSString *rainProbDayThreeValue = [NSString stringWithFormat:@"%.0f%%", [self.convertDataClass convertToPercentage:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:3] objectForKey:@"precipProbability"] doubleValue]]];
    
    NSString *summaryLabel = [NSString stringWithFormat:@"%@", [[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:0] objectForKey:@"summary"]];
    
    NSMutableArray *dailyConditionsArray = [[NSMutableArray alloc] initWithObjects:maxTemp, minTemp, sunRiseTime, sunSetTime, moonPhaseText, moonPhaseIcon, rainProbToday, rainProbTomorrow, rainProbDayTwo, rainProbDayThree, rainProbTodayValue, rainProbTomorrowValue, rainProbDayTwoValue, rainProbDayThreeValue, summaryLabel, nil];
    
    [self.weatherDataDelegate passArrayForDailyConditions:dailyConditionsArray];
    
    // ==== RainFall Values ====
    NSMutableDictionary *rainFallDictionary = [NSMutableDictionary dictionary];
    NSMutableArray *rainHourArray = [[NSMutableArray alloc] init];
    NSMutableArray *rainHourTempArray = [[NSMutableArray alloc] init];
    NSMutableArray *rainHourIconArray = [[NSMutableArray alloc] init];

    for (int x = 0; x <= 11; x++) {
        
        NSString *rainHour = [NSString stringWithFormat:@"%@", [self.convertDataClass convertToHourFromUNIX:[[[[[data objectForKey:@"hourly"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"time"] doubleValue]]];
        NSString *rainHourTemp = [NSString stringWithFormat:@"%.0fC", [self.convertDataClass convertToCelsius:[[[[[data objectForKey:@"hourly"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"apparentTemperature"] doubleValue]]];
        UIImage *rainHourIcon = [self.convertDataClass setIconImages:[[[[data objectForKey:@"hourly"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"icon"] iconSize:@"icon"];
        
        [rainHourArray addObject:rainHour];
        [rainHourTempArray addObject:rainHourTemp];
        [rainHourIconArray addObject:rainHourIcon];
        
    }
    
    [rainFallDictionary setObject:rainHourArray forKey:@"rainHour"];
    [rainFallDictionary setObject:rainHourTempArray forKey:@"rainHourTemp"];
    [rainFallDictionary setObject:rainHourIconArray forKey:@"rainHourIcon"];
    
    [self.weatherDataDelegate passDictionaryForRainfall:rainFallDictionary];
    
    // ==== Day Forecast ====
    NSMutableDictionary *dayForecastDictionary = [NSMutableDictionary dictionary];
    NSMutableArray *dayNameArray = [[NSMutableArray alloc] init];
    NSMutableArray *dayIconArray = [[NSMutableArray alloc] init];
    NSMutableArray *dayMaxTempArray = [[NSMutableArray alloc] init];
    NSMutableArray *dayMinTempArray = [[NSMutableArray alloc] init];
    
    for (int x = 0; x <= 5; x++) {
        
        NSString *dayName = [[self.convertDataClass getDay:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"time"] doubleValue]] uppercaseString];;
        UIImage *dayIcon = [self.convertDataClass setIconImages:[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"icon"] iconSize:@"icon"];
        NSString *dayMaxTemp = [NSString stringWithFormat:@"%.0f", [self.convertDataClass convertToCelsius:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"apparentTemperatureMin"] doubleValue]]];
        NSString *dayMinTemp =[NSString stringWithFormat:@"%.0f", [self.convertDataClass convertToCelsius:[[[[[data objectForKey:@"daily"] objectForKey:@"data"] objectAtIndex:x] objectForKey:@"apparentTemperatureMax"] doubleValue]]];
        
        [dayNameArray addObject:dayName];
        [dayIconArray addObject:dayIcon];
        [dayMaxTempArray addObject:dayMaxTemp];
        [dayMinTempArray addObject:dayMinTemp];
        
    }
    
    [dayForecastDictionary setObject:dayNameArray forKey:@"dayName"];
    [dayForecastDictionary setObject:dayIconArray forKey:@"dayIcon"];
    [dayForecastDictionary setObject:dayMaxTempArray forKey:@"dayMaxTemp"];
    [dayForecastDictionary setObject:dayMinTempArray forKey:@"dayMinTemp"];
    
    [self.weatherDataDelegate passDictionaryForDayForecast:dayForecastDictionary];
    
    // ==== Update Map Location ====
    double latitude = [[data objectForKey:@"latitude"] doubleValue];
    double longitude = [[data objectForKey:@"longitude"] doubleValue];
    
    NSLog(@"All data passed...");
    
    [self.weatherDataDelegate updateMapViewWithLatitude:latitude withLongitude:longitude];
    
    
    // ==== Sun Position ====
    NSInteger degreesToAdd = [self.convertDataClass makeSunPoint:[timeLabelString doubleValue] sunRise:[sunRiseTime doubleValue] sunSet:[sunSetTime doubleValue]];
    
    [self.weatherDataDelegate plotSunPosition:degreesToAdd];

    
}

@end