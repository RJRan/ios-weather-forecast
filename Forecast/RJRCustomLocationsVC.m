//
//  RJRCustomLocationsVC.m
//  Forecast
//
//  Created by Robert Randell on 02/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRCustomLocationsVC.h"
#import "RJRWeatherData.h"

@interface RJRCustomLocationsVC ()

@property (nonatomic) RJRWeatherData *weatherData;

@end

@implementation RJRCustomLocationsVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
        
    self.customLocationNames = [NSMutableArray array];
    self.customLocationLatitude = [NSMutableArray array];
    self.customLocationLongitude = [NSMutableArray array];
    
    if ([self.customLocationNames count] == 0) {
        [self.customLocationNames addObject:@"Current Location"];
        [self.customLocationLatitude addObject:[NSNumber numberWithDouble:self.customLatitude]];
        [self.customLocationLongitude addObject:[NSNumber numberWithDouble:self.customLongitude]];
    }
    
    self.customLocationsDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.customLocationNames, @"customName", self.customLocationLatitude, @"customLatitude", self.customLocationLongitude, @"customLongitude", nil];
    
    self.selectedPlace = [self.customLocationNames objectAtIndex:0];
    
    NSLog(@"Custom Locations: %@", self.customLocationsDictionary);

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.customLocationNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *cellLocation = [self.customLocationNames objectAtIndex:indexPath.row];
    
    cell.textLabel.text = cellLocation;
    
    if ([self isCurrentLocation:cellLocation]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    self.selectedPlace = cell.textLabel.text;
    float latitude = [[self.customLocationLatitude objectAtIndex:indexPath.row] doubleValue];
    float longitude = [[self.customLocationLongitude objectAtIndex:indexPath.row] doubleValue];
    
    if (self.locationDelegate != nil) {
        [self.locationDelegate locationUpdateWithLatitude:latitude withLongitude:longitude];
    } else {
        NSLog(@"Delegate is nil.");
    }
    
    [self.tableView reloadData];
        
}

- (BOOL)isCurrentLocation:(NSString *)location {
    if ([self.selectedPlace isEqualToString:location]) {
        return YES;
    }
    return NO;
}


- (IBAction)done:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
