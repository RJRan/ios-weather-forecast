//
//  RJRConversions.h
//  Forecast
//
//  Created by Robert Randell on 17/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RJRConversions : NSObject

@property (nonatomic) CGFloat arcRadius;
@property (nonatomic) CGFloat startAngle;
@property (nonatomic) CGFloat endAngle;

- (double)convertToCelsius:(double)fahrenheit;
- (double)convertToPercentage:(double)decimal;
- (NSString *)convertFromUNIX:(double)UNIXTime;
- (NSString *)convertToHourFromUNIX:(double)UNIXTime;
- (NSString *)convertMoonPhase:(double)moonValue;
- (UIImage *)convertMoonPhaseIcon:(double)moonValue;
- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
- (NSString *)getAdvanceDays:(double)UNIXTime advance:(int)amount;
- (UIImage *)setIconImages:(NSString *)condition iconSize:(NSString *)iconSize;
- (NSString *)setCurrentCondition:(NSString *)condition;
- (NSString *)setWindDirection:(double)windBearing;
- (NSString *)getDay:(double)UNIXTime;

//Arc Methods
- (NSInteger)makeSunPoint:(double)UNIXTime sunRise:(double)riseTime sunSet:(double)setTime;

@end
