//
//  RJRViewController.m
//  Forecast
//
//  Created by Robert Randell on 11/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRViewController.h"

@interface RJRViewController () <UIScrollViewDelegate, MKMapViewDelegate>

@property (nonatomic) RJRWeatherData *weatherData;
@property (nonatomic) RJRCustomLocationsVC *customLocations;

@end

@implementation RJRViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //NSLog(@"VDL...");
    
    self.mainScrollView.delegate = self;
    self.mainScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 1348);
        
    self.weatherMap.delegate = self;
    self.weatherMap.mapType = MKMapTypeHybrid;
    
    [self.rainScrollView setScrollEnabled:YES];
    [self.moonPhaseIcon setContentMode:UIViewContentModeCenter];
    [self.mainIcon setContentMode:UIViewContentModeCenter];
    self.backgroundImageBlur.alpha = 0.0;
    
    float initialAngle = 0.0174532925 * 71;
    self.sunIcon.center = CGPointMake(self.sunIcon.center.x, self.sunIcon.center.y);
    self.sunIcon.transform = CGAffineTransformMakeRotation(-initialAngle);
    
    self.locationLabel.shadowColor = [UIColor grayColor];
    self.locationLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    self.locationRegionLabel.shadowColor = [UIColor grayColor];
    self.locationRegionLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    self.timeLabel.shadowColor = [UIColor grayColor];
    self.timeLabel.shadowOffset = CGSizeMake(0.0, 1.0);
    
    self.weatherData = [[RJRWeatherData alloc] initWithAPIKey:@"35bcfad69cf6113bff0fea81926e7d6a"];
    self.weatherData.weatherDataDelegate = self;
    
    [self.weatherData getLocation];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    //NSLog(@"VDA...");
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UINavigationController *destination = (UINavigationController *)segue.destinationViewController;
    RJRCustomLocationsVC *customLocations = (RJRCustomLocationsVC *)destination.topViewController;
    RJRViewController *initialView = [segue sourceViewController];
    
    customLocations.locationDelegate = initialView;
    customLocations.customLatitude = initialView.currentLatitude;
    customLocations.customLongitude = initialView.currentLongitude;
    
}

- (void)locationUpdateWithLatitude:(double)latitude withLongitude:(double)longitude {
    
    [self.weatherData getFixedLocationAtLatitude:latitude atLongitude:longitude];
    
    NSLog(@"Location Update...");
}

- (void)updateLocationLabel:(NSString *)location locationRegionLabel:(NSString *)locationRegion; {
    
    self.locationLabel.text = location;
    self.locationRegionLabel.text = locationRegion;
    
}

- (void)passArrayForCurrentConditions:(NSMutableArray *)array {
    
    self.apparentTempLabel.text = [array objectAtIndex:0];
    self.humanityLabel.text = [array objectAtIndex:1];
    self.iconLabel.text = [array objectAtIndex:2];
    self.timeLabel.text = [array objectAtIndex:3];
    self.windSpeedLabel.text = [array objectAtIndex:4];
    self.windDirLabel.text = [array objectAtIndex:5];
    self.nextDayLabel.text = [array objectAtIndex:6];
    self.secondDayLabel.text = [array objectAtIndex:7];
    self.pressureLabel.text = [array objectAtIndex:8];
    
    self.backgroundImage.image = [array objectAtIndex:9];
    self.backgroundImageBlur.image = [array objectAtIndex:10];
    self.mainIcon.image = [array objectAtIndex:11];
    
    //NSLog(@"Delegate method Completed for Current Conditions with %@", array);

}

- (void)passArrayForDailyConditions:(NSMutableArray *)array {
    
    //NSLog(@"Delegate method Called for Daily Conditions");
    
    self.maxTemp.text = [array objectAtIndex:0];
    self.minTemp.text = [array objectAtIndex:1];
    
    self.sunRiseTime.text = [array objectAtIndex:2];
    self.sunSetTime.text = [array objectAtIndex:3];
    self.moonPhaseText.text = [array objectAtIndex:4];
    self.moonPhaseIcon.image = [array objectAtIndex:5];
    
    self.rainProbToday.image = [array objectAtIndex:6];
    self.rainProbTomorrow.image = [array objectAtIndex:7];
    self.rainProbDayTwo.image = [array objectAtIndex:8];
    self.rainProbDayThree.image = [array objectAtIndex:9];
    
    self.rainProbTodayValue.text = [array objectAtIndex:10];
    self.rainProbTomorrowValue.text = [array objectAtIndex:11];
    self.rainProbDayTwoValue.text = [array objectAtIndex:12];
    self.rainProbDayThreeValue.text = [array objectAtIndex:13];
    
    self.summaryLabel.text = [array objectAtIndex:14];
    
    //NSLog(@"Delegate method Completed for Daily Conditions");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (self.mainScrollView == scrollView) {
        float alphaValue = ((self.mainScrollView.contentOffset.y) / self.view.bounds.size.height) + 0.1;
        self.backgroundImageBlur.alpha = alphaValue;
        //NSLog(@"Main Scroll Pos: %f, Height: %f", alphaValue, self.view.bounds.size.height);
        
        if (alphaValue < 0.0) {
            //[self initQueryForData];
        }
    }
}

- (void)updateMapViewWithLatitude:(double)latitude withLongitude:(double)longitude {
    
    MKCoordinateRegion region;
    region.center.latitude = latitude;
    region.center.longitude = longitude;
    region.span.latitudeDelta = 0.3;
    region.span.longitudeDelta = 0.3;
    region = [self.weatherMap regionThatFits:region];
    [self.weatherMap setRegion:region animated:NO];
    
    self.currentLatitude = latitude;
    self.currentLongitude = longitude;
    
}

- (void)animateWindmill:(double)windSpd {
    
    float minusWind = 0;
    
    if (windSpd == 0) {
        minusWind = 0;
        //NSLog(@"Wind Speed: %.3f", minusWind);
    }
    else {
        minusWind = 250/windSpd;
        //NSLog(@"Wind Speed: %.3f", minusWind);
        
        CABasicAnimation *fullRotationAnimation;
        fullRotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        fullRotationAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
        fullRotationAnimation.toValue = [NSNumber numberWithFloat:2 * M_PI];
        fullRotationAnimation.duration = minusWind;
        fullRotationAnimation.repeatCount = MAXFLOAT;
        
        [self.windMillView.layer addAnimation:fullRotationAnimation forKey:@"360"];
        
    }
}

- (void)plotSunPosition:(NSInteger)degreesToAdd {
    
    float newAngle = 0.0174532925 * degreesToAdd;
    self.sunIcon.center = CGPointMake(self.sunIcon.center.x, self.sunIcon.center.y);
    
    [UIView animateWithDuration:2.0
                          delay:2.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGAffineTransform rotateTrans = CGAffineTransformMakeRotation(newAngle);
                         self.sunIcon.transform = rotateTrans;
                     }
                     completion:nil];
} 

- (void)passDictionaryForDayForecast:(NSMutableDictionary *)dictionary {
    
    //NSLog(@"Populating 5-Day Forecast using: %@", data);
    //NSLog(@"Delegate method Called for 5 Day Forecast");

    [self.forcastDayView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int forecastRowHeight = 29;
    
    for (int x = 1; x <= 5; x++) {
        
        UIView *dayRowView = [[UIView alloc] initWithFrame:CGRectMake(0, x * forecastRowHeight - forecastRowHeight, 270, forecastRowHeight)];
        dayRowView.backgroundColor = [UIColor clearColor];

        UILabel *day = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, forecastRowHeight)];
        [day setFont:[UIFont systemFontOfSize:12]];
        day.textColor = [UIColor whiteColor];
        day.textAlignment = NSTextAlignmentLeft;
        day.text = [NSString stringWithFormat:@"%@", [[dictionary objectForKey:@"dayName"] objectAtIndex:x]];
        
        UIImageView *conditionDayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(120, 0, 30, forecastRowHeight)];
        conditionDayIcon.backgroundColor = [UIColor clearColor];
        [conditionDayIcon setContentMode:UIViewContentModeCenter];
        conditionDayIcon.image = [[dictionary objectForKey:@"dayIcon"] objectAtIndex:x];
        
        UILabel *minTempDay = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 40, forecastRowHeight)];
        [minTempDay setFont:[UIFont systemFontOfSize:12]];
        minTempDay.textColor = [UIColor colorWithRed:0.556 green:0.827 blue:0.964 alpha:1.0];
        minTempDay.textAlignment = NSTextAlignmentRight;
        minTempDay.text = @"12";
        minTempDay.text = [NSString stringWithFormat:@"%@", [[dictionary objectForKey:@"dayMinTemp"] objectAtIndex:x]];
        
        UILabel *maxTempDay = [[UILabel alloc] initWithFrame:CGRectMake(223, -1, 40, forecastRowHeight)];
        [maxTempDay setFont:[UIFont systemFontOfSize:15]];
        maxTempDay.textColor = [UIColor whiteColor];
        maxTempDay.textAlignment = NSTextAlignmentRight;
        maxTempDay.text = @"32";
        maxTempDay.text = [NSString stringWithFormat:@"%@", [[dictionary objectForKey:@"dayMaxTemp"] objectAtIndex:x]];
        
        UIImageView *rule = [[UIImageView alloc] initWithFrame:CGRectMake(0, forecastRowHeight, 270, 1)];
        rule.backgroundColor = [UIColor whiteColor];
        rule.alpha = 0.5f;
        
        [dayRowView addSubview:day];
        [dayRowView addSubview:conditionDayIcon];
        [dayRowView addSubview:minTempDay];
        [dayRowView addSubview:maxTempDay];
        [dayRowView addSubview:rule];
        [self.forcastDayView addSubview:dayRowView];
        
    }
    
    //NSLog(@"Delegate method Complete for 5 Day Forecast");

}


- (void)passDictionaryForRainfall:(NSMutableDictionary *)dictionary {
    
   // NSLog(@"Delegate method Called for Rainfall");
    
    [self.rainScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    int rainIconWidth = 55;
    
    self.rainScrollView.contentSize = CGSizeMake(rainIconWidth * 12, 55);
    
    for (int x = 0; x <= 11; x++) {
                
        UIView *rainView = [[UIView alloc] initWithFrame:CGRectMake(rainIconWidth * x, 0, rainIconWidth, 55)];
        
        UILabel *hour = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, rainIconWidth, 15)];
        [hour setFont:[UIFont systemFontOfSize:12]];
        hour.textColor = [UIColor whiteColor];
        hour.textAlignment = NSTextAlignmentCenter;
        hour.text = [NSString stringWithFormat:@"%@", [[dictionary objectForKey:@"rainHour"] objectAtIndex:x]];
        
        UILabel *hourTemp = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, rainIconWidth, 15)];
        [hourTemp setFont:[UIFont systemFontOfSize:10]];
        hourTemp.textColor = [UIColor whiteColor];
        hourTemp.textAlignment = NSTextAlignmentCenter;
        hourTemp.text = [NSString stringWithFormat:@"%@", [[dictionary objectForKey:@"rainHourTemp"] objectAtIndex:x]];
        
        UIImageView *hourIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 55, 25)];
        hourIcon.backgroundColor = [UIColor clearColor];
        [hourIcon setContentMode:UIViewContentModeCenter];
        hourIcon.image = [[dictionary objectForKey:@"rainHourIcon"] objectAtIndex:x];

        [rainView setBackgroundColor:[UIColor clearColor]];
        [rainView addSubview:hour];
        [rainView addSubview:hourTemp];
        [rainView addSubview:hourIcon];
        [self.rainScrollView addSubview:rainView];

    }
   // NSLog(@"Delegate method Complete for Rainfall");

}

@end