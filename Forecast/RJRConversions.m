//
//  RJRConversions.m
//  Forecast
//
//  Created by Robert Randell on 17/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRConversions.h"
#import "RJRWeatherData.h"

@implementation RJRConversions

- (double)convertToCelsius:(double)fahrenheit {
    
    return ((fahrenheit - 32) * 0.555555555);
    
}

- (double)convertToPercentage:(double)decimal {
    
    return decimal * 100;
    
}

- (NSString *)convertFromUNIX:(double)UNIXTime {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:UNIXTime];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    return [dateformatter stringFromDate:date];
    
}

- (NSString *)convertToHourFromUNIX:(double)UNIXTime {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:UNIXTime];
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    return [dateformatter stringFromDate:date];
    
}

- (NSString *)getDay:(double)UNIXTime {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:UNIXTime];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    components.day = 0;
    
    [dateFormatter setLocale: [NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    return [dateFormatter stringFromDate:newDate];
}

- (NSString *)getAdvanceDays:(double)UNIXTime advance:(int)amount {
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:UNIXTime];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    components.day = amount;
    
    [dateFormatter setLocale: [NSLocale currentLocale]];
    [dateFormatter setDateFormat:@"EEEE"];
    
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    return [dateFormatter stringFromDate:newDate];
}

- (NSString *)convertMoonPhase:(double)moonValue {
    
    NSString *moonPhase;
    
    if (moonValue == 0) {
        moonPhase = @"New Moon";
    }
    else if ((moonValue >= 0.01) && (moonValue <= 0.24)) {
        moonPhase = @"Waxing Crescent";
    }
    else if (moonValue == 0.25) {
        moonPhase = @"First Half";
    }
    else if ((moonValue >= 0.26) && (moonValue <= 0.49)) {
        moonPhase = @"Waxing Gibbous";
    }
    else if (moonValue == 0.5) {
         moonPhase = @"Full Moon";
    }
    else if ((moonValue >= 0.51) && (moonValue <= 0.74)) {
        moonPhase = @"Waning Gibbous";
    }
    else if (moonValue == 0.75) {
        moonPhase = @"Last Half";
    }
    else {
        moonPhase = @"Waning Crescent";
    }
    return moonPhase;
    
}

- (NSString *)setCurrentCondition:(NSString *)condition {
    
    NSString *currentCondition = nil;
    
    if ([condition isEqualToString:@"clear-day"]) {
        currentCondition = @"Clear";
    }
    else if ([condition isEqualToString:@"clear-night"]) {
        currentCondition = @"Clear";
    }
    else if ([condition isEqualToString:@"rain"]) {
        currentCondition = @"Showers";
    }
    else if ([condition isEqualToString:@"snow"]) {
        currentCondition = @"Snow Showers";
    }
    else if ([condition isEqualToString:@"sleet"]) {
        currentCondition = @"Sleet";
    }
    else if ([condition isEqualToString:@"wind"]) {
        currentCondition = @"Windy";
    }
    else if ([condition isEqualToString:@"fog"]) {
        currentCondition = @"Foggy";
    }
    else if ([condition isEqualToString:@"cloudy"]) {
        currentCondition = @"Cloudy";
    }
    else if ([condition isEqualToString:@"partly-cloudy-day"]) {
        currentCondition = @"Partly Cloudy";
    }
    else {
        currentCondition = @"Partly Cloudy";
    }
    
    return [NSString stringWithFormat:@"%@", currentCondition];
    
}

- (UIImage *)convertMoonPhaseIcon:(double)moonValue {
    
    NSString *moonPhaseIcon;
    
    if (moonValue == 0) {
        moonPhaseIcon = @"moonphase-new";
    }
    else if ((moonValue >= 0.01) && (moonValue <= 0.24)) {
        moonPhaseIcon = @"moonphase-firstquarter";
    }
    else if (moonValue == 0.25) {
        moonPhaseIcon = @"moonphase-firsthalf";
    }
    else if ((moonValue >= 0.26) && (moonValue <= 0.49)) {
        moonPhaseIcon = @"moonphase-waxing";
    }
    else if (moonValue == 0.5) {
        moonPhaseIcon = @"moonphase-full";
    }
    else if ((moonValue >= 0.51) && (moonValue <= 0.74)) {
        moonPhaseIcon = @"moonphase-waning";
    }
    else if (moonValue == 0.75) {
        moonPhaseIcon = @"moonphase-lasthalf";
    }
    else {
        moonPhaseIcon = @"moonphase-lastquarter";
    }

    return [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", moonPhaseIcon]];
    
}

- (UIImage *)setIconImages:(NSString *)condition iconSize:(NSString *)iconSize {
    
    NSString *imageName = nil;
    
    if ([condition isEqualToString:@"clear-day"]) {
        imageName = @"clearsky";
    }
    else if ([condition isEqualToString:@"clear-night"]) {
        imageName = @"clearnight";
    }
    else if ([condition isEqualToString:@"rain"]) {
        imageName = @"rain";
    }
    else if ([condition isEqualToString:@"snow"]) {
        imageName = @"snow";
    }
    else if ([condition isEqualToString:@"sleet"]) {
        imageName = @"sleet";
    }
    else if ([condition isEqualToString:@"wind"]) {
        imageName = @"wind";
    }
    else if ([condition isEqualToString:@"fog"]) {
        imageName = @"fog";
    }
    else if ([condition isEqualToString:@"cloudy"]) {
        imageName = @"cloudy";
    }
    else if ([condition isEqualToString:@"partly-cloudy-day"]) {
        imageName = @"partlycloudy";
    }
    else {
        imageName = @"partlycloudynight";
    }
    
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@-%@", imageName, iconSize]];
   
}

- (NSString *)setWindDirection:(double)windBearing {
    
    NSString *windDirection = nil;
    
    if ((windBearing >= 0) && (windBearing <= 22)) {
        windDirection = @"N";
    }
    else if ((windBearing >= 23) && (windBearing <= 44)) {
        windDirection = @"NNE";
    }
    else if ((windBearing >= 45) && (windBearing <= 77)) {
        windDirection = @"NE";
    }
    else if ((windBearing >= 78) && (windBearing <= 89)) {
        windDirection = @"NEE";
    }
    else if ((windBearing >= 90) && (windBearing <= 112)) {
        windDirection = @"E";
    }
    else if ((windBearing >= 113) && (windBearing <= 134)) {
        windDirection = @"SEE";
    }
    else if ((windBearing >= 135) && (windBearing <= 157)) {
        windDirection = @"SE";
    }
    else if ((windBearing >= 158) && (windBearing <= 179)) {
        windDirection = @"SSE";
    }
    else if ((windBearing >= 180) && (windBearing <= 202)) {
        windDirection = @"S";
    }
    else if ((windBearing >= 203) && (windBearing <= 234)) {
        windDirection = @"SSW";
    }
    else if ((windBearing >= 235) && (windBearing <= 257)) {
        windDirection = @"SW";
    }
    else if ((windBearing >= 258) && (windBearing <= 270)) {
        windDirection = @"SWW";
    }
    else if ((windBearing >= 270) && (windBearing <= 292)) {
        windDirection = @"W";
    }
    else if ((windBearing >= 293) && (windBearing <= 314)) {
        windDirection = @"NWW";
    }
    else if ((windBearing >= 315) && (windBearing <= 347)) {
        windDirection = @"NW";
    }
    else {
        windDirection = @"NWW";
    }
    
    return [NSString stringWithFormat:@"%@", windDirection];
}

- (UIImage *)maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef), CGImageGetHeight(maskRef), CGImageGetBitsPerComponent(maskRef), CGImageGetBitsPerPixel(maskRef), CGImageGetBytesPerRow(maskRef), CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef maskedImageRef = CGImageCreateWithMask([image CGImage], mask);
    UIImage *maskedImage = [UIImage imageWithCGImage:maskedImageRef];
    
    CGImageRelease(mask);
    CGImageRelease(maskedImageRef);
    
    return maskedImage;
}

#pragma mark - Plot Sun Position

- (NSInteger)makeSunPoint:(double)UNIXTime sunRise:(double)riseTime sunSet:(double)setTime {
    
    //NSLog(@"Params - Time Now: %.0f, Sunrise: %.0f, Sunset: %.0f", UNIXTime, riseTime, setTime);
    
    NSInteger sunPoint = 0;
    NSInteger initialDegrees = -71;
    NSInteger totalDegreesToSpan = 142;
    
    if (UNIXTime < setTime) {
        
        double dayLength = setTime - riseTime;
        double dayLengthNow = setTime - UNIXTime;
        double dayPercentage = (dayLengthNow / dayLength);
        double dayRemains = (1 - dayPercentage);
        double degreesNow = totalDegreesToSpan * dayRemains;
        
        sunPoint = initialDegrees + degreesNow;
        
    }
    else {
        sunPoint = 180;
    }
    return sunPoint;
}

@end
