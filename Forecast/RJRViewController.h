//
//  RJRViewController.h
//  Forecast
//
//  Created by Robert Randell on 11/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "RJRWeatherData.h"
#import "RJRCustomLocationsVC.h"

@interface RJRViewController : UIViewController <dataDelegate, locationUpdateDelegate>

@property (nonatomic, assign) CLLocationDegrees currentLatitude;
@property (nonatomic, assign) CLLocationDegrees currentLongitude;

@property (nonatomic, retain) NSMutableDictionary *weatherDataFromAPI;

@property (nonatomic) IBOutlet UILabel *apparentTempLabel;
@property (nonatomic) IBOutlet UILabel *humanityLabel;
@property (nonatomic) IBOutlet UILabel *iconLabel;
@property (nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic) IBOutlet UILabel *windSpeedLabel;
@property (nonatomic) IBOutlet UILabel *windDirLabel;
@property (nonatomic) IBOutlet UILabel *locationLabel;
@property (nonatomic) IBOutlet UILabel *locationRegionLabel;
@property (nonatomic) IBOutlet UILabel *currentMoonPhase;
@property (nonatomic) IBOutlet UILabel *maxTemp;
@property (nonatomic) IBOutlet UILabel *minTemp;
@property (nonatomic) IBOutlet UILabel *nextDayLabel;
@property (nonatomic) IBOutlet UILabel *secondDayLabel;
@property (nonatomic) IBOutlet UILabel *pressureLabel;
@property (nonatomic) IBOutlet UILabel *summaryLabel;

@property (nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic) IBOutlet UIScrollView *rainScrollView;
@property (nonatomic) IBOutlet MKMapView *weatherMap;

@property (weak, nonatomic) IBOutlet UIView *forcastDayView;
@property (weak, nonatomic) IBOutlet UIView *mainContentView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageBlur;
@property (weak, nonatomic) IBOutlet UIImageView *mainIcon;

@property (weak, nonatomic) IBOutlet UIImageView *rainProbToday;
@property (weak, nonatomic) IBOutlet UIImageView *rainProbTomorrow;
@property (weak, nonatomic) IBOutlet UIImageView *rainProbDayTwo;
@property (weak, nonatomic) IBOutlet UIImageView *rainProbDayThree;
@property (weak, nonatomic) IBOutlet UIImageView *windMillView;
@property (weak, nonatomic) IBOutlet UIImageView *sunIcon;

@property (weak, nonatomic) IBOutlet UILabel *rainProbTodayValue;
@property (weak, nonatomic) IBOutlet UILabel *rainProbTomorrowValue;
@property (weak, nonatomic) IBOutlet UILabel *rainProbDayTwoValue;
@property (weak, nonatomic) IBOutlet UILabel *rainProbDayThreeValue;

@property (weak, nonatomic) IBOutlet UIImageView *windDirectionIcon;
@property (weak, nonatomic) IBOutlet UIImageView *moonPhaseIcon;
@property (weak, nonatomic) IBOutlet UILabel *moonPhaseText;
@property (weak, nonatomic) IBOutlet UILabel *sunRiseTime;
@property (weak, nonatomic) IBOutlet UILabel *sunSetTime;

@end