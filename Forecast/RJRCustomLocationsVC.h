//
//  RJRCustomLocationsVC.h
//  Forecast
//
//  Created by Robert Randell on 02/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

@protocol locationUpdateDelegate <NSObject>

@required
- (void)locationUpdateWithLatitude:(double)latitude withLongitude:(double)longitude;

@end

#import <UIKit/UIKit.h>

@interface RJRCustomLocationsVC : UITableViewController

@property (nonatomic, weak) id <locationUpdateDelegate> locationDelegate;

@property (nonatomic) NSMutableDictionary *customLocationsDictionary;
@property (nonatomic) NSMutableArray *customLocationNames;
@property (nonatomic) NSMutableArray *customLocationLatitude;
@property (nonatomic) NSMutableArray *customLocationLongitude;

@property (nonatomic) NSString *selectedPlace;
@property (nonatomic) double customLatitude;
@property (nonatomic) double customLongitude;

- (IBAction)done:(id)sender;

@end
