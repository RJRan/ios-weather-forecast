//
//  RJRCustomPlacesVC.m
//  Forecast
//
//  Created by Robert Randell on 17/07/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

#import "RJRCustomPlacesVC.h"

@interface RJRCustomPlacesVC ()

@end

@implementation RJRCustomPlacesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
