//
//  RJRWeatherData.h
//  Forecast
//
//  Created by Robert Randell on 11/06/2014.
//  Copyright (c) 2014 Robert Randell. All rights reserved.
//

@protocol dataDelegate <NSObject>

@required
- (void)updateLocationLabel:(NSString *)location locationRegionLabel:(NSString *)locationRegion;
- (void)passArrayForCurrentConditions:(NSMutableArray *)array;
- (void)passArrayForDailyConditions:(NSMutableArray *)array;
- (void)passDictionaryForRainfall:(NSMutableDictionary *)dictionary;
- (void)passDictionaryForDayForecast:(NSMutableDictionary *)dictionary;
- (void)updateMapViewWithLatitude:(double)latitude withLongitude:(double)longitude;
- (void)animateWindmill:(double)windSpd;
- (void)plotSunPosition:(NSInteger)degreesToAdd;

@end

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <CoreLocation/CoreLocation.h>

@interface RJRWeatherData : NSObject

@property (nonatomic, weak) id <dataDelegate> weatherDataDelegate;

@property (nonatomic) AFHTTPSessionManager *manager;
@property (nonatomic, retain) NSMutableDictionary *weatherDataFromAPI;
@property (nonatomic) NSString *apiKey;

// ==== Location Strings ====
@property (nonatomic, retain) NSString *locationLabel;
@property (nonatomic, retain) NSString *locationRegionLabel;
@property (nonatomic, retain) NSString *placeName;
@property (nonatomic, retain) NSString *regionName;

- (id)initWithAPIKey:(NSString *)api_key;
- (void)getCurrentConditionsForLatitude:(double)lat longitude:(double)lon success:(void (^)(NSMutableDictionary *responseDict))success failure:(void (^)(NSError *error))failure;
- (void)getFixedLocationAtLatitude:(double)latitude atLongitude:(double)longitude;
- (void)getLocation;

@end
